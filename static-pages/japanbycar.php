<?php
/**
* Template Name: Japan By Car Project Page
* Description: Look pah! I made a template!
*/ 
get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php
    $hero_title = "Japan By Car";
    $hero_subtitle = "";
    $hero_image = get_template_directory_uri()."/ressources/images/japanbycar/header.jpg";
    $small_hero = false;
    include(locate_template('page-header.php'));
    ?>

    <div class="about-air">
      <div class="wrap">
        <h2>What is Japan By Car ?</h2>
        <p class="two-columns-text">
          ものづくり文化が浸透する街、アメリカ・オレゴン州ポートランドで自分の表現を追求する二人のアメリカ人陶芸家が来日し、三重県伊賀市丸柱に滞在しながら作陶活動を行う「shifteast アーティスト・イン・レジデンス。
          陶芸家のミカ・デマルケスとアレクサンドリア・カミングスが丸柱で生活をしながら伊賀焼きの歴史や文化を学び、里山の自然を感じ、地元の人々と交流していきます。
          そしてその全てを吸収し、丸柱の土と窯を使い、ここでしか生まれない作品を作り上げて行きます。
        </p>
      </div>
    </div>

    <div class="artists">
      <div class="wrap">
        <h2>The Artists</h2>
        <div class="artist-container">
            <div class="artist-portrait" data-bg="<?php echo get_template_directory_uri(); ?>/ressources/images/air/alexandria.jpg"></div>
            <div class="artist-name">
              Alexandria Cummings
            </div>
            <div class="artist-katakana-name">
              アレクサンドリア・カミングス
            </div>
            <div class="artist-infos">
              <p>
                陶芸作家。ポートランドのコミュニティースタジオ「Hey Studio」を共同運営。
                毎日の食卓で使うのが楽しくなるような、シンプルで洗練された食器やマグカップを製作。
                全ての器は彼女自身の手によって作られ、ポートランドの人気レストランでも使われている。
              </p>
            </div>
          </div>
          <div class="artist-container">
            <div class="artist-portrait" data-bg="<?php echo get_template_directory_uri(); ?>/ressources/images/air/mica.jpg"></div>
            <div class="artist-name">
              Mica Demarquez
            </div>
            <div class="artist-katakana-name">
              ミカ・デマルケス
            </div>
            <div class="artist-infos">
              <p>
                陶芸作家。自身の陶器ブランド「Mimi Ceramics」のマグカップや器は、
                日々の暮らしで使える実用性と機能性を兼ね備え、特徴的な色や模様でデザインも探求。
                いずれの器もろくろでの形づくりから本焼きまで全て本人による手作業で生み出されている。
              </p>
            </div>
          </div>
      </div>
    </div>

    <?php 
  $white = 'white';
  include(locate_template('newsletter-section.php'));
  ?>
  </main>
</div>

<?php
get_footer();