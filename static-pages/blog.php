<?php
/**
* Template Name: Blog Page
* Description: Look pah! I made a template!
*/ 
get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

    <?php
    $hero_title = "Blog";
    $hero_subtitle = "";
    $hero_image = "";
    $small_hero = true;
    include(locate_template('page-header.php'));
    ?>

    <?php
    $args = array(
        'post_type'      => 'post',
        'posts_per_page' => -1
    );

    $query = new WP_Query($args);
    ?>

    <div class="content">

    <?php
    if ( $query->have_posts() ) : ?>

        <div class="container">
            <div class="row">
                <?php while ( $query->have_posts() ) : $query->the_post();?>
                    <?php get_template_part( 'components/listed-post' );?>
                <?php endwhile; ?>
            </div><!-- row -->
        </div><!-- ontainer -->

    <?php else :?>
        <?php get_template_part( 'template-parts/content', 'none' );?>
    <?php endif; ?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();