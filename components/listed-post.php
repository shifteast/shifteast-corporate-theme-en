<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shifteast
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('listed-article-container'); ?>>
    <div class="article-card">
        <a href="<?php echo the_permalink(); ?>" class="header-image">
            <div class="background" data-bg="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>"></div>
        </a>
        <div class="card-body">
            <div class="author-container">
                <div class="author-portrait" data-bg="<?php echo get_avatar_url($post->post_author); ?>">

                </div>
                Written by <?php the_author(); ?>
            </div>
            <a href="<?php echo the_permalink(); ?>" class="article-title"><?php the_title(); ?></a>
            <p>
                <?php the_excerpt(); ?>
            </p>
            <div style="text-align: center;">
                <a href="<?php echo the_permalink(); ?>" class="vueform-btn big filled" style="margin: auto;">Read Article</a>
            </div>
        </div>
    </div>
</div>