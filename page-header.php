<div class="page-hero <?php if($small_hero) { echo "small-hero"; } ?>">
    <div class="background" data-bg="<?php echo $hero_image; ?>"></div> 
    <div class="content">
        <h1><?php echo $hero_title; ?></h1>
        <div class="subtitle"><?php echo $hero_subtitle; ?></div>
    </div>
    <?php if(!$small_hero) { ?>
    <div class="scroll-downs">
        <div class="mousey">
            <div class="scroller"></div>
        </div>
    </div>
    <?php } ?>
</div>